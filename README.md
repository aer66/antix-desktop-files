# antiX desktop files

This resource was created from the translations of all strings found in .desktop files from antiX 21 and 19, present on [antiX-contribs at transifex](https://www.transifex.com/antix-linux-community-contributions/antix-contribs/antix-desktop-files/), comprising all .desktop files found in 21 and 19 full ISO, many of them lacking of complete translation sets still.

It is meant to provide multilanguage support for antiX in as many languages as possible. For now there are ca.100 languages available. If you find your language missing, just ask for it at [antiX forums](forum.antixlinux.com). We'll do our very best to add it.

The resource strings will be updated as needed.

By now most of the languages are machine translated only in large part. So in case you find a wording of a translation to your language inconvenient, just register for free at transifex, join our translation team and improve it, by simply replacing the existing translation with your improvement, using e.g. the transifex online translation editor in your browser. Your improvements will get incorporated in next update of the resource, and this way available to all antiX users of your language.

## Authors and acknowledgment
This is an antiX community project, 2022

## License
Licensed under the same license as antiX and the projects from which the original strings found in the  processed .desktop files originate.

## Project status
Just started. First translation run accomplished.
